var gulp = require('gulp'),
    browserSync = require('browser-sync'),
	imagemin = require('gulp-imagemin'),
	cssmin = require('gulp-clean-css'),
	uglify = require('gulp-uglify'),
	minify = require('gulp-minify'),
	htmlreplace = require('gulp-html-replace'),
    concat = require('gulp-concat'),
	postcss = require('gulp-postcss'),
    typograf = require('gulp-typograf');
  
 // watch Task 
gulp.task('watch', function () {
  var files = [
    'www/*.html',
	'www/img/*',
    'www/css/*.css',
   
  ];
    browserSync.init(files, {
    server: {
      baseDir: './www'
    }
  });
  
  //gulp.watch('www/img/*', ['compress']);
   
});

// Compress img Task
gulp.task('img', function() {
gulp.src('./www/img/**/*')
    .pipe(imagemin({
      progressive: true
    }))
    .pipe(gulp.dest('./build/img/'));
});

// Compress CSS Task 
gulp.task('css', function () {
return gulp.src(['./www/css/*.css']) // путь к файлам-исходникам
    .pipe(concat('index.css')) // прогоняем их через плагин
	.pipe(cssmin())
    .pipe(gulp.dest('./build/css')) // путь к папке, куда помещаем конечные файлы
});


// replace Task  in html  <!-- build:js --><!-- endbuild --><!-- build:css --><!-- endbuild -->
gulp.task('replace', function() {
  gulp.src(['./build/*.php','./build/*.html'])
    .pipe(htmlreplace({
        'css': 'css/index.css'     
    }))
    .pipe(gulp.dest('./build/'));
});

gulp.task('fonts', function () {
    return gulp.src(['./www/fonts/*/*']) // путь к файлам-исходникам
    .pipe(gulp.dest('./build/fonts')) // путь к папке, куда помещаем конечные файлы
});

gulp.task('typograf', function() {
    gulp.src(['./www/*.html'])
      /* .pipe(typograf({ 
 locale: ['ru', 'en-US'],
            htmlEntity: { type: 'digit' },
            safeTags: [
                ['<\\?php', '\\?>']
            ],
			// Own rules
    rules: [
        {
            name: 'common/other/typographicalCustom',
            handler: function(text, settings) {
                return text.replace(' при ', ' при ');
        }
        },
		 {
            name: 'common/other/typographicalCustom2',
            handler: function(text, settings) {
                return text.replace(' для ', ' для ');
        }
        },
		{
            name: 'common/other/typographicalCustom3',
            handler: function(text, settings) {
                return text.replace(' под ', ' под ');
        }
		},
		{
            name: 'common/other/typographicalCustom3',
            handler: function(text, settings) {
                return text.replace( '&#8381;', '&#8381;');
        }
        }
    ]

		}))  */
		 .pipe(htmlreplace({ 
		 // replace Task  in html  <!-- build:js --><!-- endbuild --><!-- build:css --><!-- endbuild -->
        'css': 'css/index.css',
		'js': 'js/index-min.js'		
    }))
        .pipe(gulp.dest('./build/'));

});


// concat JS 
gulp.task('js', function () {
   return gulp.src(['./www/js/libs.min.js', './www/js/main.js']) // путь к файлам-исходникам

    .pipe(concat('index.js')) // прогоняем их через плагин
 .pipe(minify())
    .pipe(gulp.dest('./build/js')) // путь к папке, куда помещаем конечные файлы

});


// Compress JS  
gulp.task('jscomp', function () {
   return gulp.src(['./build/js/index.js']) // путь к файлам-исходникам
   
   .pipe(minify())
   .pipe(gulp.dest('./build/js/')) // путь к папке, куда помещаем конечные файлы
});

gulp.task('copy', function () {
    return gulp.src(['./www/js/swiper-bundle.min.js']) // путь к файлам-исходникам
    .pipe(gulp.dest('./build/js/')) // путь к папке, куда помещаем конечные файлы
});

// bild Task
gulp.task('build', function () {
	gulp.start('typograf');
	gulp.start('css');
	gulp.start('js');
	gulp.start('fonts');
	gulp.start('img');
	gulp.start('copy');
	//gulp.start('jscomp');


	});

// Default Task
gulp.task('default', ['watch']);