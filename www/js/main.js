"use strict";

let element = document.body; //check IE

if (!window.ActiveXObject && "ActiveXObject" in window) {
    element.classList.add("ie");

    if ('NodeList' in window && !NodeList.prototype.forEach) {
        console.info('polyfill for IE11 forEach');

        NodeList.prototype.forEach = function(callback, thisArg) {
            thisArg = thisArg || window;

            for (let i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }
}

function ValidMail(input_id) {
    let re = /^[a-zA-Zа-яА-ЯёЁ0-9&-_\.]+@[a-zA-Zа-яА-ЯёЁ0-9&_-]+\.[a-zA-Zа-яА-ЯёЁ0-9_]{2,10}$/i;
    let myMail = input_id.value; //alert(myMail);

    let valid = re.test(myMail);

    if (valid) {
        //input_id.parents('.form_item').removeClass('form_item_error');
        console.log('valid');
        input_id.parentElement.parentElement.classList.remove("formitem_error");
    } else {
        //input_id.parents('.form_item').addClass('form_item_error');
        console.log('novalid');
        input_id.parentElement.parentElement.classList.add("formitem_error");
    }

    return valid;
}

document.addEventListener('DOMContentLoaded', function() {
    //forms
    document.querySelectorAll(".formitem").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();
            this.classList.add("formitem_focus");
            this.querySelector(".js-focus").focus();
        }, true);
        el.addEventListener('blur', function(e) {
            let tmpval = this.querySelector(".js-focus").value;

            if (this.querySelector(".js-email")) {
                ValidMail(this.querySelector(".js-email"));
            }

            if (tmpval != '') {
                this.classList.add("formitem_hasvalue");
            } else {
                this.classList.remove("formitem_hasvalue");
            }

            this.classList.remove("formitem_focus");
        }, true);
        el.addEventListener('focus', function(e) {
            this.classList.add("formitem_focus");
        }, true);
    });

    //authform show-hide-password

    document.querySelectorAll(".js-formitem__show-hide-password").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();

            if (this.classList.contains('show')) {
                this.classList.remove("show");
                this.parentElement.querySelector(".js-focus").setAttribute('type', 'password');
            } else {
                this.classList.add("show");
                this.parentElement.querySelector(".js-focus").setAttribute('type', 'text');
            }
        });
    });

    //authform go to reestablish

    document.querySelectorAll(".js-authform-go-reestablish-btn").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();
            document.querySelectorAll(".js-authform-steps").forEach(function(elstep) {
                elstep.classList.add("none");
            });
            document.querySelector(".js-authform-reestablish").classList.remove("none");
        });
    });

    //authform go to login

    document.querySelectorAll(".js-authform-go-login-btn").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();
            document.querySelectorAll(".js-authform-steps").forEach(function(elstep) {
                elstep.classList.add("none");
            });
            document.querySelector(".js-authform-login").classList.remove("none");
        });
    });

    //layout. Open / close aside
    if (document.querySelector(".js-open-aside")) {
        document.querySelector(".js-open-aside").addEventListener('click', function(e) {
            event.preventDefault();

            if (document.querySelector(".js_page_main").classList.contains('page_main_open')) {
                document.querySelector(".js_page_main").classList.remove("page_main_open");
            } else {
                document.querySelector(".js_page_main").classList.add("page_main_open");
            }
        });
    }


    //layout. close psevdoselect

    let closestByClass = function(el, clazz) {
        // Traverse the DOM up with a while loop
        while (el.className != clazz) {
            // Increment the loop to the parent node
            el = el.parentNode;

            if (!el) {
                return null;
            }
        } // At this point, the while loop has stopped and `el` represents the element
        // that has the class you specified in the second parameter of the function
        // `clazz`


        return el;
    };

    //layout. list messages

    if (document.querySelector(".header-layout__list-messages")) {
        document.addEventListener('click', function(e) {
            if (closestByClass(e.target, 'header-layout__messages-ico') || closestByClass(e.target, 'header-layout__list-messages show') || closestByClass(e.target, 'header-layout__list-messages')) {
                let wasopen = 0;

                if (document.querySelector(".header-layout__list-messages").classList.contains('show')) {
                    wasopen = 1;
                }

                document.querySelector(".header-layout__list-messages").classList.add("show");
            } else {
                document.querySelector(".header-layout__list-messages").classList.remove("show");
            }
        });
    }
    //layout. psevdoselect

    document.addEventListener('click', function(e) {
        if (closestByClass(e.target, 'psevdoselect') || closestByClass(e.target, 'psevdoselect open') || closestByClass(e.target, 'psevdoselect psevdoselect-num') || closestByClass(e.target, 'psevdoselect psevdoselect-num open')) {
            let wasopen = 0;

            if (e.target.closest(".psevdoselect").classList.contains('open')) {
                wasopen = 1;
            }


            console.log(wasopen);


            document.querySelectorAll(".psevdoselect").forEach(function(el) {
                el.classList.remove("open");
            });

            if (e.target.classList.contains('js_psevdoselect_item')) {
                let mytext = e.target.innerText || e.target.textContent;
                console.log('Клик по значению: ' + mytext);
                e.target.closest(".psevdoselect").querySelector(".js_psevdoselect__txt").innerHTML = mytext;
                e.target.closest(".psevdoselect").querySelectorAll(".js_psevdoselect_item").forEach(function(el) {
                    el.classList.remove("active");
                });
                e.target.classList.add("active");
            }

            if (wasopen == 0) {
                e.target.closest(".psevdoselect").classList.add("open");
            } else {
                e.target.closest(".psevdoselect").classList.remove("open");
            }
        } else {
            document.querySelectorAll(".psevdoselect").forEach(function(el) {
                el.classList.remove("open");
            });
        }
    });

    //layout. popups

    document.querySelectorAll(".js-open-popup").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();
            let need = this.getAttribute('data-popup');
            document.querySelector(".shadow").classList.remove("open");
            document.querySelectorAll(".popup").forEach(function(elstep) {
                elstep.classList.remove("open");
            });
            let scrolltp = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop;
            document.querySelector(".shadow").classList.add("open");
            document.querySelector(".popup." + need).classList.add("open");
            document.querySelector("body").setAttribute('data-scroll', scrolltp);
            document.querySelector("body").classList.add("body__popup_open");
            document.querySelector(".page__wrapper").style.marginTop = '-' + scrolltp + 'px';
        });
    });

    //layout. popups close

    document.querySelectorAll(".js-closepopup").forEach(function(el) {
        el.addEventListener('click', function(e) {
            event.preventDefault();
            document.querySelectorAll(".popup").forEach(function(elstep) {
                elstep.classList.remove("open");
            });
            document.querySelector(".shadow").classList.remove("open");
            document.querySelector("body").classList.remove("body__popup_open");
            let needed_scroll = document.querySelector("body").getAttribute('data-scroll');
            console.log(needed_scroll);
            document.querySelector(".page__wrapper").style.marginTop = '-' + 0 + 'px';
            window.scrollTo({
                top: needed_scroll
            });
        });
    });




    // File upload 

    if (document.getElementById("files__upload-fileInput")) {

        let fileInput = document.getElementById('files__upload-fileInput');

        function formatBytes(bytes, decimals = 2) {
            if (bytes === 0) return '0 Bytes';
            const k = 1024;
            const dm = decimals < 0 ? 0 : decimals;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            const i = Math.floor(Math.log(bytes) / Math.log(k));
            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        }

        function handleFileSelect(evt) {
            evt.stopPropagation();
            evt.preventDefault();
            let files = evt.target.files; // FileList object
            let output = [];
            let outputName = [];
            for (let i = 0, f; f = files[i]; i++) {
                output.push('<li><strong>', (f.name), '</strong> (', f.type || 'n/a', ') - ',
                    formatBytes(f.size), ' bytes, last modified: ',
                    f.lastModifiedDate.toLocaleDateString(), '</li>');
                outputName.push('<div class="f_name">', (f.name), '</div><div class="f_size">', formatBytes(f.size), '</div>');
            }

            //document.querySelector(".files__upload-btn_countainer").innerHTML = '' + outputName.join('') + '';
            document.querySelector(".files__upload-result").innerHTML = '' + outputName.join('') + '';
			 document.querySelector(".files__upload-result_countainer").classList.remove("none");
			 document.querySelector(".files__upload-dropzone").classList.add("none");
			 document.querySelector(".files__upload-btn_countainer").classList.add("none");

			if (document.querySelector(".js-send_upl")) {
				document.querySelector(".js-send_upl").classList.remove("disabled");
			}


        }

        function handleFileDropSelect(evt) {
            evt.stopPropagation();
            evt.preventDefault();
            let files = evt.dataTransfer.files; // FileList object.
            fileInput.files = files;
            let output = [];
            let outputName = [];
            for (let i = 0, f; f = files[i]; i++) {
                output.push('<li><strong>', (f.name), '</strong> (', f.type || 'n/a', ') - ',
                    formatBytes(f.size), ' bytes, last modified: ',
                    f.lastModifiedDate.toLocaleDateString(), '</li>');

                outputName.push('<div class="f_name">', (f.name), '</div><div class="f_size">', formatBytes(f.size), '</div>');
            }
            //document.querySelector(".files__upload-btn_countainer").innerHTML = '' + outputName.join('') + '';
            document.querySelector(".files__upload-result").innerHTML = '' + outputName.join('') + '';

			 document.querySelector(".files__upload-result_countainer").classList.remove("none");
			 document.querySelector(".files__upload-dropzone").classList.add("none");
			 document.querySelector(".files__upload-btn_countainer").classList.add("none");
			
			if (document.querySelector(".js-send_upl")) {
				document.querySelector(".js-send_upl").classList.remove("disabled");
			}


        }

        function handleDragOver(evt) {
            evt.stopPropagation();
            evt.preventDefault();
            evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
        }

        // Setup the dnd listeners.
        document.querySelector(".js-files__upload-dropzone").addEventListener('click', function(e) {
            event.preventDefault();
            fileInput.click();
        });
        let dropZone = document.getElementById('files__upload-dropzone');
        dropZone.addEventListener('dragover', handleDragOver, false);
        dropZone.addEventListener('drop', handleFileDropSelect, false);
        fileInput.addEventListener('change', handleFileSelect, false);

        // js-files__upload-result_remove
        document.querySelector(".js-files__upload-result_remove").addEventListener('click', function(e) {
            event.preventDefault();
			document.querySelector(".files__upload-result_countainer").classList.add("none");
			document.querySelector(".files__upload-dropzone").classList.remove("none");
			document.querySelector(".files__upload-btn_countainer").classList.remove("none");
		fileInput.value = '';
			if (document.querySelector(".js-send_upl")) {
				document.querySelector(".js-send_upl").classList.add("disabled");
			}


        });



    }




});